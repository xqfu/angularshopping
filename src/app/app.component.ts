import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor (
    public authService: AuthService
  ) {}
  title = 'AngularShopping';
  // https://medium.com/@moshevilner/creating-shopping-cart-using-angular8-c6a5d4a0f0b3
  productList = [
    {name: 'Z900', price: 8799},
    {name: 'shubert helmet', price: 999},
    {name: 'sport gloves', price: 99}
   ];
  cartProductList = [];
}
