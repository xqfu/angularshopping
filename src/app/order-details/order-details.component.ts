//@ts-nocheck
import { Component, Input, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  // @Input('orderId')
  // myOrderId;

  public orderDetails: orderDetail[];
  public my_username : string = "";
  public my_orderid : string = "";
  public my_orderprice : any;


  constructor(public orderService: OrderService) { }


  ngOnInit(): void {
    this.orderService.findOrderDetails().subscribe(data => {  
      console.log(data);
      this.orderDetails = data;
      console.log(this.orderDetails);
    });


    if (localStorage.getItem('My_username')) {
      this.my_username = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }    
    if (localStorage.getItem('My_orderid')) {
      this.my_orderid = localStorage.getItem('My_orderid').replace("'", "").replace(/\"/g, "");
    }
    if (localStorage.getItem('My_orderprice')) {
      this.my_orderprice = localStorage.getItem('My_orderprice').replace("'", "").replace(/\"/g, "");
    }

    
    //localStorage.setItem('My_orderprice', JSON.stringify(order.totalPrice));
  }
}
