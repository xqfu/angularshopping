import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import {AppGuard} from './app.guard';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { UserListComponent } from './user-list/user-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { PDetailsComponent } from './p-details/p-details.component';
import { PAddComponent } from './p-add/p-add.component';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { ChangepwdComponent } from './changepwd/changepwd.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    canActivate: [AppGuard],
    children: [              
      {
        path: 'cart',
        component: CartComponent
      },            
      {
        path: 'order',
        component: OrderComponent
      },                      
      {
        path: 'productlist',
        component: ProductListComponent
      },                         
      {
        path: 'productdetail',
        component: PDetailsComponent
      },                               
      {
        path: 'productadd',
        component: PAddComponent
      },               
      {
        path: 'userlist',
        component: UserListComponent
      }, 
      {
        path: 'logout',
        component: LogoutComponent
      }
    ] 
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'product',
    component: ProductComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }, 
  {
    path: 'changepwd',
    component: ChangepwdComponent
  },              
  {
    path: 'userdetail',
    component: UserdetailComponent
  },                       
  {
    path: 'orderdetails',
    component: OrderDetailsComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
