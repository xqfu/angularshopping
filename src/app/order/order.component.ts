//@ts-nocheck
import { Component, VERSION, OnInit, Input, Output, EventEmitter, OnChanges, forwardRef} from '@angular/core';
//import { CartService } from 'src/app/services/cart.service';
import { OrderService } from '../services/order.service';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent implements OnInit, OnChanges {
  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(true);
  response;

  public orders: order[];
  public my_username : string = "";
  public my_userid : string = "";
  public my_userrole : string = "";
  
  constructor(public orderService: OrderService) { }

  ngOnInit() {
      // let username;
      // if (localStorage.getItem('My_username'))
      //   username = localStorage.getItem('My_username');
      // console.log(username);
      // let userId;
      // if (localStorage.getItem('My_userId'))
      //   userId = localStorage.getItem('My_userId');
      // console.log(userId);
      // let user;
      // if (localStorage.getItem('My_user'))
      //   user = localStorage.getItem('My_user');
      // console.log(user.username);      
      // 2
      //this.orderService.findCurrentUserOrders().subscribe(data => {
        this.orderService.findCurrentUserOrders().subscribe(data => {  
         this.orders = data;
     });

     if (localStorage.getItem('My_username')) {
      this.my_username = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }
    if (localStorage.getItem('My_userid')) {
      this.my_userid = localStorage.getItem('My_userid').replace("'", "").replace(/\"/g, "");
    }
    if (localStorage.getItem('My_userrole')) {
      this.my_userrole = localStorage.getItem('My_userrole').replace("'", "").replace(/\"/g, "");
    }
  }

  pages: number = 1;
  // dataset: any[] = ['Oder 1 Description ','Oder 2 Description ','Oder 3 Description ','Oder 4 Description ','Oder 5 Description ','Oder 6 Description ','Oder 7 Description ','Oder 8 Description ','Oder 9 Description ','Oder 10 Description ','Oder 11 Description ','Oder 12 Description ','Oder 13 Description ','Oder 14 Description ','Oder 15 Description ','Oder 16 Description ','Oder 17 Description ','Oder 18 Description ','Oder 19 Description ','Oder 20 Description '];
  
  // constructor() { }
  toOrderDetails(order: any) {
    //return this.http.get<Order[]>(this.API_URL + "/orderdetail?orderId="+this.order.orderId);
    //this.router.navigate(['/orderdetails']);
    this.orderService.toOrderDetails(order);
    //alert("order=");
    // console.log(order); 
    // this.router.navigate(['/orderdetails']);
  }
  
  removeOrder(order: any) {
    if (confirm('Are you sure you want to delete the order and its details?')) {
      // Save it!
        this.orderService.removeOrder(order);     
        console.log('The order was deleted!');  
        window.location.reload(); 
        this.loggedIn.next(true);
    } else {
      // Do nothing!
      //alert('The order was not deleted!');
      console.log('The order was not deleted!');
    }
    
    //alert("removeOrder");
    //console.log(order); 
  }

      //function to remove item 
      // removeItem(item: any){
      //   this.cartService.removeCartItem(item);
      // }
    
      // // function to remove all items at once
      // emptyCart(){
      //   this.cartService.removeAllCart();
      // }

      // getOrderDetails(order: any) {
      //   //return this.http.get<Order[]>(this.API_URL + "/orderdetail?orderId="+this.order.orderId);
      //   //this.router.navigate(['/orderdetails']);
      //   //alert("order=");
      //   console.log(order);   
      //   return this.http.get<Order[]>(this.API_URL + "/orderdetail?orderId="+this.order.orderId);
      //   this.router.navigate(['/orderdetails']);
      // }
      
      // removeOrder(order: any) {
      //   //alert("removeOrder");
      //   console.log(order);   
      // }
}
