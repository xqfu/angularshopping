// import { ComponentFixture, TestBed,async } from '@angular/core/testing';
// import { RouterTestingModule } from '@angular/router/testing';
// import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
// import {HttpClientModule} from '@angular/common/http';
// import { OrderService } from '../services/order.service';

// import { OrderComponent } from './order.component';

// describe('OrderComponent', () => {
//   let component: OrderComponent;
//   let fixture: ComponentFixture<OrderComponent>;
//   let service: OrderService;
//   let httpMock: HttpTestingController;  

//   beforeEach(async () => {
//     TestBed.configureTestingModule({
//       imports: [RouterTestingModule, HttpClientTestingModule],
//       providers: [OrderService]
//     })
//     service = TestBed.get(OrderService);
//     httpMock = TestBed.get(HttpTestingController); 
//   });
  
//   afterEach(() => {
//     httpMock.verify();
//   })  

//   //   .compileComponents();

//   //   fixture = TestBed.createComponent(OrderComponent);
//   //   component = fixture.componentInstance;
//   //   fixture.detectChanges();
//   // });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   // it('should render the text in h3 tag', () => { //
//   //   expect(fixture.debugElement.nativeElement.querySelector('h3').textContent).toContain('Orders');
//   // });


// });

import { ComponentFixture, TestBed,async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';

import { OrderComponent } from './order.component';

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpClientTestingModule
      ],
      declarations: [ OrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the text in h3 tag', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('h3').textContent).toContain('Orders');
  });


});