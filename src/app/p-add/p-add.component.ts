//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
@Component({
  selector: 'app-p-add',
  templateUrl: './p-add.component.html',
  styleUrls: ['./p-add.component.css']
})
export class PAddComponent implements OnInit {

  constructor(public productService: ProductService,
    public router: Router) { }

  ngOnInit(): void {
  }

  addProduct(product) {
    console.log(product);
    this.productService.addProduct(product)
    .subscribe((res) => {
      // if (res.success) {
      //   alert("The product is added!");
      //   this.router.nagivate(['/productlist']);
      // } else
      // alert("The product is not added!");
    });    
    this.router.nagivate(['/productlist']);
  }

}
