import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { PAddComponent } from './p-add.component';
import { FormsModule } from '@angular/forms';

describe('PAddComponent', () => {
  let component: PAddComponent;
  let fixture: ComponentFixture<PAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ PAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });  
  
  it('should render the text in h2 tag', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Add a new product');
  });
});
