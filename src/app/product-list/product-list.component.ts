//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  response;

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(true);


  public products: product[];
  public my_username : string = "";
  public my_userrole : string = "";
//
  constructor(public router: Router, public pService: ProductService) { }

  ngOnInit() {
   
    let userRole: string = "";
    if (localStorage.getItem('My_userrole')) {
      userRole = localStorage.getItem('My_userrole').replace("'", "").replace(/\"/g, "");
    }
    console.log("userRole="+userRole);
    //this.authService.getCurrentUserRole().subscribe(data => this.data = data);
    //userRole = await this.authService.getCurrentUserRole();
    
    // // this.authService.getCurrentUserRole().subscribe(data => {  
    // //   console.log(data);
    // //   userRole = data;
    // //   console.log("userRole="+userRole);
    // // });

    if (userRole === "ROLE_ADMIN") {
      this.pService.findAll().subscribe(data => {
        this.products = data;
      });  
    } else {
      alert("Authentication failed!");
      console.log("Authentication failed!");
      this.loggedIn.next(true);
      this.router.navigate(['/product']);
    }

    this.my_userrole = userRole;
    if (localStorage.getItem('My_username')) {
      this.my_username = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }


  } 

  removeProduct(product: any) {
    if (confirm('Are you sure you want to delete the product?')) {
        //let sku=product.sku;
        this.pService.removeProduct(product); 
        //this.router.nagivate(['/productlist']);       
        //this.router.navigateByUrl('/productlist')
        location.reload();
    } else {
      // Do nothing!
      //alert('The product was not deleted!');
      console.log('The product was not deleted!');
    }    
  }

  editProduct(sku, name, imageUrl, description, unitPrice) {
    //console.log("product="+product.id);  product.name, product.imageUrl, product.description, product.unitPrice
    localStorage.setItem('product_sku', JSON.stringify(sku));
    localStorage.setItem('product_name', JSON.stringify(name));    
    console.log("name="+name);
    localStorage.setItem('product_imageUrl', JSON.stringify(imageUrl));
    console.log("imageUrl="+imageUrl);
    localStorage.setItem('product_description', JSON.stringify(description));
    console.log("description="+description);
    localStorage.setItem('product_unitPrice', JSON.stringify(unitPrice));
    //this.router.nagivate(['/productdetail']);
    this.router.navigateByUrl('/productdetail')
  }
  
}