export class OrderDetailOrig {
    public orderId: number = 0;
    public productId: number = 0;
    public amount: number = 0;

    constructor(orderId: number, productId: number, amount: number) {
        this.orderId = orderId;
        this.productId = productId;
        this.amount = amount;       
    }
  }