export class Product {
    public id: number = 0;
    public sku: string = "";
    public name: string = "";
    public description: string = "";
    public unitPrice: number = 0.0;
    public imageUrl: string = "";
 

  }