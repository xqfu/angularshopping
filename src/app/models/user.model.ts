export class User {
    public id: number = 0;
    public username: string = "";
    public password: string = "";
    public phone: string = "";
    public email: string = "";
    public address1: string = "";
    public address2: string = "";
    public city: string = "";
    public state: string = "";
    public zip: string = "";

    // <!-- phone, email, address1, address2, city, state, zip -->
  }
