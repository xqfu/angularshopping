import { ComponentFixture, fakeAsync, TestBed, tick, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { of, Subject } from 'rxjs';

describe('AppComponent', () => {
  let mockAuthService: any;
  let fixture: any;
  let mockIsAdmin: any

  beforeEach(async () => {
    mockAuthService = jasmine.createSpyObj(['checkLogin', 'checkIsAdmin']);
    // create a mock isAdmin subject 
    mockAuthService.isAdmin = new Subject<boolean>();

    mockAuthService.checkLogin.and.returnValue(of([{ success: true}]));
    mockAuthService.isAdmin.next(true)

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpClientTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AngularShopping'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('AngularShopping');
  });

  // it("should call checkLogin in authService once", fakeAsync(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges() //ngOnInit

  //   //expect(mockAuthService.checkLogin).toHaveBeenCalled();    

  // }))
  
  // it('should render title', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.nativeElement as HTMLElement;
  //   console.log(compiled.querySelector('.content span')?.textContent);
  //   alert(compiled.querySelector('.content span')?.textContent);
  //   expect(compiled.querySelector('.content span')?.textContent).toContain('AngularShopping');
  // });
});
