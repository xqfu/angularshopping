import { ComponentFixture, fakeAsync, TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
//import {HttpClientModule} from '@angular/common/http';
import { LogoutComponent } from './logout.component';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { of, Subject } from 'rxjs';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let isLoggedIn: boolean;
  let authService: AuthService;
  let mockAuthService: any;
  let mockRouter: any;
  let router: Router;
  let http: HttpClient;
  

  beforeEach(async () => {
    mockAuthService = jasmine.createSpyObj("authService", ['logout', 'getUserDetail'], { isLoggedIn: new Subject()} )
    mockAuthService.logout.and.returnValue(of({ success: true }))
    mockAuthService.getUserDetail.and.returnValue(of({ email: "test", firstName:"test"}))
    mockAuthService.isLoggedIn.next(true)
    mockRouter = { navigate: jasmine.createSpy('navigate')}
    
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [ LogoutComponent ],
      providers: [
          { provide: AuthService, useValue: mockAuthService }, 
          { provide: Router, useValue: mockRouter}
        ]
      })
    .compileComponents();

    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(LogoutComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  //   //router = fixture.debugElement.injector.get(Router);
  // });



  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should render the text in p tag', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('p').textContent).toContain('You will leave Angular Shopping system!');
  });
	it('should render button Click to logout', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Click to logout');
  });

  it('should click button', async(() => {
    let buttonElement = fixture.debugElement.nativeElement.querySelector('button'); 
    buttonElement.click();
    fixture.detectChanges();  
    fixture.whenStable().then(() => {

    });   

  })); 
  
  it("should logout", fakeAsync(() => {
         component.logout();
        //spyOn(component, 'logout');
        fixture.detectChanges();    
        //expect(mockRouter.navigate).toHaveBeenCalledOnceWith(['/login'])
    
        let response;    
        authService = new AuthService(http, router);
        // authService.checklogin().subscribe( res => {
        //   response = res;
        //   expect(res.success);
        // });
    
        spyOn(authService, "checklogin");
        fixture.detectChanges();   
        // authService.checklogin().subscribe( res => {
        // //   //response = res;
        // //   expect(res);
        // });
        //expect(authService.logout()).toHaveBeenCalled();
    //  expect(mockRouter.navigate).toHaveBeenCalledOnceWith(['/login']);
    //component.logout();
    //expect(component.logout).toHaveBeenCalled();
    //mockAuthService = jasmine.createSpyObj("authService", [], {});
    // mockAuthService.logout;
     //expect(mockAuthService.logout()).toHaveBeenCalled();
    // const spy = router.navigateByUrl as jasmine.Spy;
    // const navArgs = spy.calls.first().args[0];
    // expect(component).toBeTruthy();
    // expect(navArgs).toBe('/login');
     //expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Login');
     //expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Click to login');

  }));


});
