//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import {User} from 'src/app/models/user.model';

@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.css']
})
export class ChangepwdComponent implements OnInit {
  public my_username : string = "";
  response;

  constructor(public authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('My_username')) {
      this.my_username = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }
  }

  changepwd(cpwd) {
    console.log("cpwd="+cpwd);
    console.log("cpwd.username="+cpwd.username);
    console.log("cpwd.oldpassword="+cpwd.oldpassword);
    console.log("cpwd.newpassword="+cpwd.newpassword);
    console.log("cpwd.confirmpassword="+cpwd.confirmpassword);
    if (cpwd.newpassword != cpwd.confirmpassword) {
      alert("New passwords do not match!");      
    } else {
      let user = new User();
      user.username=cpwd.username;
      user.password=cpwd.oldpassword;
      this.authService.simplelogin(user)
      .subscribe((res) => {
        this.response = res;
        if (res.success) {
          user.password=cpwd.newpassword;
          this.authService.changepwd(user)
          .subscribe((res) => {
            this.response = res;
            if (res.success) {
              alert("Your password is changed!");  
            }
            else {
              alert("Your password is not changed!");  
            }
          });
        } else {
          alert("Your username or old password issues!");  
        }
      });      
    }
  }
}