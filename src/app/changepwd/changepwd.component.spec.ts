import { ComponentFixture, TestBed,async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ChangepwdComponent } from './changepwd.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
describe('ChangepwdComponent', () => {
  let component: ChangepwdComponent;
  let fixture: ComponentFixture<ChangepwdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule], 
      declarations: [ ChangepwdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangepwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
  it('should render the text in h2 tag', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Change Password');
  });
	it('should render button Click to change your password', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Click to change your password');
  });

  it('should click button', async(() => {
    let buttonElement = fixture.debugElement.nativeElement.querySelector('button'); 
    buttonElement.click();
    fixture.detectChanges();  
    fixture.whenStable().then(() => {
    });
  })); 

});
