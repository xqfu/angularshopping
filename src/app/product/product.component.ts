//@ts-nocheck
import { Component, VERSION, OnInit, Input, Output, EventEmitter, OnChanges, forwardRef  } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from '../services/product.service';
//  https://www.delftstack.com/zh/howto/angular/pagination-in-angular/
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit, OnChanges {
  response;

  public products: product[];
  
  constructor(public cartService: CartService, public pService: ProductService) { }

  ngOnInit() {
       this.pService.findAll().subscribe(data => {
         this.products = data;
     });
    
  }
  pages: number = 1;
  // dataset: any[] = ['Product 1 Description ','Product 2 Description ','Product 3 Description ','Product 4 Description ','Product 5 Description ','Product 6 Description ','Product 7 Description ','Product 8 Description ','Product 9 Description ','Product 10 Description ','Product 11 Description ','Product 12 Description ','Product 13 Description ','Product 14 Description ','Product 15 Description ','Product 16 Description ','Product 17 Description ','Product 18 Description ','Product 19 Description ','Product 20 Description '];

  productList = this.products;
  // productList = [
  //   {name: 'Z900', unitPrice: 8799},
  //   {name: 'shubert helmet', unitPrice: 999},
  //   {name: 'sport gloves', unitPriceunitPrice: 99}
  //  ];
  cartProducts = [];

  addToCart(product) {
    //console.log(product);
    this.cartService.addToCart(product);
  }

  // editToCart(product) {
  //   console.log(product);
    
  // }
  // toCart() {
  //   this.response = res;
  //   this.router.navigate(['/product']);
  // }
}
