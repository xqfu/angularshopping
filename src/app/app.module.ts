import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { AppGuard } from './app.guard';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductComponent } from './product/product.component';
import { LogoutComponent } from './logout/logout.component';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { PDetailsComponent } from './p-details/p-details.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { UserListComponent } from './user-list/user-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { PAddComponent } from './p-add/p-add.component';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { ChangepwdComponent } from './changepwd/changepwd.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProductComponent,
    LogoutComponent,
    CartComponent,
    OrderComponent,
    PDetailsComponent,
    OrderDetailsComponent,
    UserListComponent,
    ProductListComponent,
    PAddComponent,
    UserdetailComponent,
    ChangepwdComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule, 
    NgxPaginationModule 
  ],
  providers: [
    AuthService,
    AppGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
