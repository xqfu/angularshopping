import { ComponentFixture, TestBed,async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { UserdetailComponent } from './userdetail.component';
import { FormsModule } from '@angular/forms';

describe('UserdetailComponent', () => {
  let component: UserdetailComponent;
  let fixture: ComponentFixture<UserdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ UserdetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should render the text in h2 tag', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Edit the user:');
  });
  
	it('should render button Submit', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Submit');
  });

  it('should click button', async(() => {
    let buttonElement = fixture.debugElement.nativeElement.querySelector('button'); 
    buttonElement.click();
    fixture.detectChanges();  
    fixture.whenStable().then(() => {
    });
  })); 

  
	it('should render click User Management', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('a').textContent).toContain('User Management');
  });

  // it('should click Url', async(() => {
  //   let aElement = fixture.debugElement.nativeElement.querySelector('a'); 
  //   aElement.click();
  //   fixture.detectChanges();  
  //   fixture.whenStable().then(() => {
  //   });
  // })); 

});
