//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import {User} from 'src/app/models/user.model';
import { AppConfig } from '../services/app.config';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css']
})
export class UserdetailComponent implements OnInit {
  // <!-- phone, email, address1, address2, city, state, zip -->
  public my_username: String ="";
  public my_phone: string = "";
  public my_email: string = "";
  public my_address1: string = "";
  public my_address2: string = "";
  public my_city: string = "";
  public my_state: string = "";
  public my_zip: string = "";
  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(public authService: AuthService,
    public router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('user_username')) {
      this.my_username = localStorage.getItem('user_username').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_username="+this.my_username);
    if (localStorage.getItem('user_phone')) {
      this.my_phone = localStorage.getItem('user_phone').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_phone="+this.my_phone);
    if (localStorage.getItem('user_email')) {
      this.my_email = localStorage.getItem('user_email').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_email="+this.my_email);
    if (localStorage.getItem('user_address1')) {
      this.my_address1 = localStorage.getItem('user_address1').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_address1="+this.my_address1);
    if (localStorage.getItem('user_address2')) {
      this.my_address2 = localStorage.getItem('user_address2').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_address2="+this.my_address2);
    if (localStorage.getItem('user_city')) {
      this.my_city = localStorage.getItem('user_city').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_city="+this.my_city);
    if (localStorage.getItem('user_state')) {
      this.my_state = localStorage.getItem('user_state').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_state="+this.my_state);
    if (localStorage.getItem('user_zip')) {
      this.my_zip = localStorage.getItem('user_zip').replace("'", "").replace(/\"/g, "");
    }
    console.log("this.my_zip="+this.my_zip);
  }

  editUser(user) {
    console.log(user);
    user.username = this.my_username;
    this.authService.editUser(user)
    .subscribe((res) => {
      // if (res.success) {
      //   alert("The user is changed!");
      //   this.router.nagivate(['/userlist']);
      // } else
      // alert("The user is not changed!");
    });
    this.loggedIn.next(true);
    this.router.nagivate(['/userlist']);
  }
}
