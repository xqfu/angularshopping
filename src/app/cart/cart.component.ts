//@ts-nocheck
import { Component, OnChanges, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { OrderService } from 'src/app/services/order.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnChanges {
  public product : any = [];
  public grandTotal !: number ;
  public my_username : string = "";

  // Injecting Cart Service 
   constructor(private cartService: CartService, private orderService:OrderService) { }
  
    // getting an instance of an observable to getList Of Products/price
    ngOnInit(): void {
      this.grandTotal = 0.0;
      this.cartService.getProducts()
      .subscribe(res=>{
        this.product= res;
        this.grandTotal = this.cartService.getTotalPrice();
        //this.grandTotal.toFixed(2);
      })

      if (localStorage.getItem('My_username')) {
        this.my_username = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
      }
      //console.log("my_userrole="+ this.my_userrole);
    }
  
    ngOnChanges(changes: SimpleChanges): void {
      //this.grandTotal.toFixed(2);
    }

    //function to remove item 
    removeItem(item: any){
      this.cartService.removeCartItem(item);
    }
  
    // function to remove all items at once
    emptyCart(){
      this.cartService.removeAllCart();
    }
		
		// checkOut(){
    //   this.cartService.removeAllCart();
    // }

    // computeProductMaps(){
    //   this.cartService.computeProductMaps();
    // }

    checkout() {
      sessionStorage.setItem('My_grandTotal', JSON.stringify(this.grandTotal));
      //this.cartService.computeProductMaps();
      //this.orderService.checkout();
      this.productMaps = this.cartService.computeProductMaps();
      //let totalPrice = 0.0;
      console.log(this.productMaps);   
      for (let [key, value] of this.productMaps) {
        console.log(key, value);            
        //totalPrice += value;
      }
      console.log("totalPrice="+this.grandTotal);
      
      // order.userId 
      // order.totalPrice
      this.orderService.checkout(this.productMaps, this.grandTotal);
      this.cartService.removeAllCart();
      
      // this.orderService.saveOrder(order)
      // .subscribe((res) => {
      //   if (res.success) {
      //     alert("Checkout passed!");
      //   } else
      //   alert("Checkout failed!");
      // });
    }
  }
  