import { ComponentFixture, TestBed, fakeAsync, inject, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { LoginComponent } from './login.component';
import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { of, Subject } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import {User} from "../models/user.model";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let isLoggedIn: boolean;
  let authService: AuthService;
  let router: Router;
  let http: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ LoginComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should render title in a h2 tag', () => { 
    expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Login');
  });

	it('should have div', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('div'));
  });
  
	it('should render button Click to login', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Click to login');
  });

  it('should click button', async(() => {
    let buttonElement = fixture.debugElement.nativeElement.querySelector('button'); 
    buttonElement.click();
    fixture.detectChanges();  
    fixture.whenStable().then(() => {
    });
  }));
  
  it("should login", fakeAsync(() => {
    let user = new User;
    user.username="xfu";
    user.password="123456";
    //spyOn(component, 'login', );
    component.login(user);
    spyOn(component, "login").withArgs(user);
    fixture.detectChanges();
    
    
    let response;    
    authService = new AuthService(http, router);
    // authService.checklogin().subscribe( res => {
    //   response = res;
    //   expect(res.success);
    // });

    // @ts-ignore
    spyOn(authService, "checklogin");
    fixture.detectChanges();
    // authService.checklogin().subscribe( res => {
    //   response = res;
    //   expect(res.success);
    // });
    //component.logout();
    //expect(component.logout).toHaveBeenCalled();
    //mockAuthService = jasmine.createSpyObj("authService", [], {});
    // mockAuthService.logout;
    //expect(authService.login(user)).toHaveBeenCalled();
    //expect(router.navigate).toHaveBeenCalledOnceWith(['/product']);
    // expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Login');
    // expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Click to login');
  }));

  // it('should click Url', async(() => {
  //   let aElement = fixture.debugElement.nativeElement.querySelector('a'); 
  //   aElement.click();
  //   fixture.detectChanges();  
  //   fixture.whenStable().then(() => {
  //   });
  // })); 

});
