//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public authService: AuthService,
    public router: Router) { }

  ngOnInit(): void {
  }

  register(user) {    
    console.log("user1="+user.phone);
    this.authService.register(user)
      .subscribe((res) => {
        if (res.success) {
          this.router.nagivate(['/login']);
        } else
        alert("Reister failed!");
      });
  }


}
