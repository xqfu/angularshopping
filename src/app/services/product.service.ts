//@ts-nocheck
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig } from './app.config';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private API_URL = AppConfig.API_URL;

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(private http: HttpClient, private router: Router) { }

  findAll(): Observable<any> {
    //this.loggedIn.next(true);
    return this.http.get<Product[]>(this.API_URL + "/products");
  }

  // findProductById(pid: any): Observable<any> {
  //   //this.loggedIn.next(true);
  //  // return this.http.get<Product[]>(this.API_URL + "/products");.map((response: Response) => response.json())
  //   //return this.http.get(this.API_URL + "/products/detail?id="+pid);
  //   return this.http.get(this.API_URL + "/products/detail?id="+pid).subscribe(data => {
  //     // console.log(data);
  //     // localStorage.setItem('My_userId', JSON.stringify(data));
  //     return data;
  //   });
  // }

  // findProductBySku(psku: any): Observable<any> {
  //   //this.loggedIn.next(true);
  //  // return this.http.get<Product[]>(this.API_URL + "/products");.map((response: Response) => response.json())
  //   //return this.http.get(this.API_URL + "/products/detail?id="+pid);
  //   return this.http.get(this.API_URL + "/productId?sku="+psku).subscribe(data => {
  //     // console.log(data);
  //     // localStorage.setItem('My_userId', JSON.stringify(data));
  //     return data;
  //   });

    
  // }

  removeProduct(product: any) {
    console.log(product);        
    //this.productId = product.id.replace("'", "").replace(/\"/g, "");
    let endPoint = product.id;
    this.http.delete(this.API_URL + "/products/"+endPoint).subscribe({
      next: data => {
          this.status = 'Delete successful';
      },
      error: error => {
          this.errorMessage = error.message;
          console.error('There was an error!', error);
      }
   });
  }

  addProduct(product): Observable<any> {
    return this.http.post(this.API_URL + "/products", product)
    .pipe(map((res) => { 
      console.log(res);
      if (res.success) {
        console.log("The product is added!");
      }  
    }));
  }

  editProduct(product): Observable<any> {
    return this.http.put(this.API_URL + "/products", product)
    .pipe(map((res) => { 
      console.log(res);
      if (res.success) {
        console.log("The product is edited!");
      }  
    }));
  }
//   private getProducts(searchUrl: string): Observable<Product[]> {
//     return this.httpClient.get<GetResponseProducts>(searchUrl).pipe(
//       map(response => response._embedded.products)
//     );
//   }

//   //returns an observable of product array
//   //maps json data from spring rest to product array
//   getProductList(theCategoryId: number): Observable<Product[]> {

//     //build URL based on category id
//     const searchUrl = `${this.baseUrl}/search/findByCategoryId?id=${theCategoryId}`;

//   return this.getProducts(searchUrl);
//   }

//   //add support for pagination
//   getProductListPaginate(thePage: number, thePageSize: number,
//                             theCategoryId: number): Observable<GetResponseProducts> {

//     //build URL based on category id for pagination
//     //Spring REST API supports pagination out the box, so just pass page # and size
//     const searchUrl = `${this.baseUrl}/search/findByCategoryId?id=${theCategoryId}`
//                     + `&page=${thePage}&size=${thePageSize}`;
//   return this.httpClient.get<GetResponseProducts>(searchUrl);
//   }


//   getProductCategories(): Observable<ProductCategory[]> {
//     return this.httpClient.get<GetResponseProductCategory>(this.categoryUrl).pipe(
//       map(response => response._embedded.productCategory)
//       );
//   }

//   searchProducts(theKeyword: string): Observable<Product[]> {
//     const searchUrl = `${this.baseUrl}/search/findByNameContaining?name=${theKeyword}`;
//     return this.getProducts(searchUrl);
//   }

//   searchProductsPaginate(thePage: number, 
//                         thePageSize: number,
//                         theKeyword: string): Observable<GetResponseProducts> {

//     //build URL based on keyword
//     //Spring REST API supports pagination out the box, so just pass page # and size
//     const searchUrl = `${this.baseUrl}/search/findByNameContaining?name=${theKeyword}`
//                       + `&page=${thePage}&size=${thePageSize}`;
//     return this.httpClient.get<GetResponseProducts>(searchUrl);
//     }

//   getProduct(theProductId: number): Observable<Product> {
//     //build URL based on product id
//     const productUrl = `${this.baseUrl}/${theProductId}`;
//     return this.httpClient.get<Product>(productUrl);
//   }


// }

// //helps unwrap JSON data
// interface GetResponseProducts {
//   _embedded: {
//     products: Product[];
//   },
//   page: { //add pagination
//     size: number,
//     totalElements: number,
//     totalPages: number,
//     number: number

//   }
}

