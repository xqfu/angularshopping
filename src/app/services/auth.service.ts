//@ts-nocheck
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig } from './app.config';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_URL = AppConfig.API_URL;

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private router: Router) { }

  simplelogin(user): Observable<any> {
    let params = new HttpParams();
    params = params.append('username', user.username);
    params = params.append('password', user.password);
    return this.http.post(this.API_URL + "/login", params)
    .pipe(map((res) => {
      this.loggedIn.next(res.success);
      return res;
    }));
  }
  
  
  login(user): Observable<any> {
    let params = new HttpParams();
    params = params.append('username', user.username);
    params = params.append('password', user.password);
    //this.createdOrderId
    let uname: string = "";
    uname = user.username;
    localStorage.setItem('My_username', JSON.stringify(uname));
    this.http.get(this.API_URL + "/userRole?username="+uname, {responseType: 'text'}).subscribe(data => {
      console.log(data);
      localStorage.setItem('My_userrole', JSON.stringify(data));
    });
   
    //localStorage.setItem('My_user', JSON.stringify(user));
    return this.http.post(this.API_URL + "/login", params)
    .pipe(map((res) => {
      this.loggedIn.next(res.success);
      if (this.loggedIn) {
        // let userId;
        //userId = this.http.get(this.API_URL + "/userId", user.username);    
        this.http.get(this.API_URL + "/userId?username="+uname, {responseType: 'text'}).subscribe(data => {
          console.log(data);
          localStorage.setItem('My_userid', JSON.stringify(data));
        });

        //console.log(userId);
        //localStorage.setItem('My_userId', JSON.stringify(userId));
        this.router.navigate(['/product']);
      }
      return res;
    }));
  }

    checklogin(): Observable<any> {
      return this.http.get(this.API_URL + "/checklogin", { withCredentials: true})
    .pipe(map((res) => {
      this.loggedIn.next(res.success);
      return res;
    }));
  }

    logout(): Observable<any> {
      return this.http.post(this.API_URL + "/logout", {}, { withCredentials: true})
    .pipe(map((res) => {  
      this.loggedIn.next(false);      
      localStorage.setItem('My_username', JSON.stringify(""));
      localStorage.setItem('My_userrole', JSON.stringify(""));
      localStorage.setItem('My_userid', JSON.stringify(""));
      localStorage.setItem('My_orderid', JSON.stringify(""));
      localStorage.setItem('My_productid', JSON.stringify(""));
      this.router.navigate(['/login']);
      return res;
    }));
  }

  register(user): Observable<any> {
    //console.log("user2="+user.phone);
    return this.http.post(this.API_URL + "/users", user)
    .pipe(map((res) => { 
      console.log(res);
      if (res.success) {
        this.router.navigate(['/login']);
      }  
    }));
  }

  editUser(user): Observable<any> {
    //console.log("user2="+user.phone);
    return this.http.put(this.API_URL + "/users", user)
    .pipe(map((res) => { 
      console.log(res);
      if (res.success) {
        //this.router.navigate(['/login']);
        console.log("The user is updated!");
      }  
    }));
  }

  changepwd(user): Observable<any> {
    //console.log("user2="+user.phone);
    return this.http.put(this.API_URL + "/users/changepwd", user)
    .pipe(map((res) => { 
      console.log(res);
      if (res.success) {
        //this.router.navigate(['/login']);
        console.log("The user is updated!");
      }  
    }));
  }

  findAll(): Observable<any> {
    //this.loggedIn.next(res.success);
    return this.http.get<User[]>(this.API_URL + "/users");
    // return this.http.get<User[]>(this.API_URL + "/users")
    // .pipe(map((res) => {  
    //   this.loggedIn.next(true);
    //   //this.router.navigate(['/login']);
    //   return User;
    // }));
  }
  
  findCurrentUser(): Observable<any> {
    //this.loggedIn.next(res.success);
    let uname: string = "";
    if (localStorage.getItem('My_username')) {
      uname = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }
    console.log("uname="+uname);
    return this.http.get<User[]>(this.API_URL + "/users/username?username="+uname);
    // return this.http.get<User[]>(this.API_URL + "/users")
    // .pipe(map((res) => {  
    //   this.loggedIn.next(true);
    //   //this.router.navigate(['/login']);
    //   return User;
    // }));
  }

  removeUser(user: any) {
    //alert("removeOrder");
    console.log(user);        
    //this.userId = user.id.replace("'", "").replace(/\"/g, "");
    let endPoint = user.id;
    this.http.delete(this.API_URL + "/users/"+endPoint).subscribe({
      next: data => {
          this.status = 'Delete successful';
      },
      error: error => {
          this.errorMessage = error.message;
          console.error('There was an error!', error);
      }
   });
  }
}
