import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductService } from './product.service';
import { of } from 'rxjs';
import {HttpClient, HttpHandler} from "@angular/common/http";
import {Product} from "../models/product.model";
import {ProductListComponent} from "../product-list/product-list.component";

describe('ProductService', () => {
  let productsrvService: ProductService;
  let httpMock = HttpTestingController;
  // @ts-ignore
  let comp : ProductListComponent = new ProductListComponent();

  beforeEach(() => {
        // @ts-ignore
        productsrvService = new ProductService();
        TestBed.configureTestingModule({
          declarations: [HttpClientTestingModule, ProductListComponent],
        providers: [
            HttpClient,
            HttpHandler
          ]
        }); //.compileComponents();
  });

  it('should be created', () => {
    expect(productsrvService).toBeTruthy();
  });

  it('test get all', () => {
    const pdts = [
        {
          "id": 1,
          "sku": "000-0001",
          "name": "Test",
          "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. ",
          "unitPrice": 9.99,
          "imageUrl": "https://www.prepressure.com/images/lorem-ipsum-samuel-generator.jpg"
        },
        {
          "id": 2,
          "sku": "000-0002",
          "name": "Pen",
          "description": "A common writing instrument that applies ink to a surface, usually paper, for writing or drawing",
          "unitPrice": 2.9,
          "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Carandache_Ecridor.jpg/220px-Carandache_Ecridor.jpg"
        },
        {
          "id": 5,
          "sku": "DEST5",
          "name": "5",
          "description": "D55",
          "unitPrice": 5.0,
          "imageUrl": "https://images.twinkl.co.uk/tw1n/image/private/t_630/image_repo/2b/2b/T-T-17828-Editable-A4-Tools_ver_1.jpg"
        },
        {
          "id": 11,
          "sku": "000-003",
          "name": "3",
          "description": "D333",
          "unitPrice": 1.3,
          "imageUrl": "https://www.bradfordexchange.com/store/20091217001/responsive2/img/confidence/guarantee.png"
        }
      ];
      let response;
      // @ts-ignore
      spyOn(productsrvService, "findAll").and.returnValue(of(pdts));
      productsrvService.findAll().subscribe( res => {
        response = res;
      });
      // @ts-ignore
      expect(response).toEqual(pdts);


    });

});
// id: number = 0;// sku: string = "";// name: string = "";// description: string = "";// unitPrice: number = 0.0;// imageUrl: string = "";
// [{"id":1,"sku":"000-0001","name":"Test","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. ","unitPrice":9.99,"imageUrl":"https://www.prepressure.com/images/lorem-ipsum-samuel-generator.jpg","active":true,"unitsInStock":1,"dateCreated":"2018-05-18 09:50:48","lastUpdated":"2020-10-22 01:55:43"},
// {"id":2,"sku":"000-0002","name":"Pen","description":"A common writing instrument that applies ink to a surface, usually paper, for writing or drawing","unitPrice":2.9,"imageUrl":"https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Carandache_Ecridor.jpg/220px-Carandache_Ecridor.jpg","active":true,"unitsInStock":1,"dateCreated":"2018-05-18 09:50:48","lastUpdated":"2020-10-22 01:55:43"},
// {"id":5,"sku":"DEST5","name":"5","description":"D55","unitPrice":5.0,"imageUrl":"https://images.twinkl.co.uk/tw1n/image/private/t_630/image_repo/2b/2b/T-T-17828-Editable-A4-Tools_ver_1.jpg","active":true,"unitsInStock":99,"dateCreated":"2023-01-10 03:23:05","lastUpdated":null},
// {"id":11,"sku":"000-003","name":"3","description":"D333","unitPrice":1.3,"imageUrl":"https://www.bradfordexchange.com/store/20091217001/responsive2/img/confidence/guarantee.png","active":true,"unitsInStock":99,"dateCreated":"2023-01-11 15:49:11","lastUpdated":null}]