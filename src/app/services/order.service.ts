//@ts-nocheck
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig } from './app.config';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router'; 
import { CartService } from 'src/app/services/cart.service';
import {Order} from 'src/app/models/order.model';
import {OrderDetailOrig} from 'src/app/models/order-detail.model';
import {Product} from 'src/app/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  public orderId: String = "";
  private API_URL = AppConfig.API_URL;
  //public createdOrderId: String = "";

  //const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(private http: HttpClient, private router: Router, private cartService:CartService) { }

  findAll(): Observable<any> {
    this.loggedIn.next(true);
    return this.http.get<Order[]>(this.API_URL + "/orders");
  }

  findCurrentUserOrders(): Observable<any> {
    //uname: string = "";
    let uname: string = "";
    if (localStorage.getItem('My_username')) {
      uname = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }
    console.log(uname);
    return this.http.get<Order[]>(this.API_URL + "/orderlist?username="+uname);    
  }

  findOrderDetails(): Observable<any> {
    //uname: string = "";
    let oid: string = "";   
    //oid= orderId; //.replace("'", "").replace(/\"/g, "");
    if (localStorage.getItem('My_orderid')) {
      oid = localStorage.getItem('My_orderid').replace("'", "").replace(/\"/g, "");
    }
    console.log(oid);
    return this.http.get<OrderDetail[]>(this.API_URL + "/orderdetails?orderId="+oid);    
  }
  // removeCartItem(product:any){
  //   this.cartItemList.map((a:any, index:any)=>{
  //     if(product.id === a.id){
  //       this.cartItemList.splice(index,1)
  //     }
  //   })
  //   this.productList.next(this.cartItemList)
  // }

  toOrderDetails(order: any) {
    //return this.http.get<Order[]>(this.API_URL + "/orderdetail?orderId="+this.order.orderId);
    //this.router.navigate(['/orderdetails']);
    //alert("order=");
    console.log(order);   
    this.orderId = order.id;    
    localStorage.setItem('My_orderid', JSON.stringify(order.id));    
    localStorage.setItem('My_orderprice', JSON.stringify(order.totalPrice));
    //return this.http.get<Order[]>(this.API_URL + "/orderdetail?orderId="+this.order.orderId);
    this.router.navigate(['/orderdetails']);
  }
  
  removeOrder(order: any) {
    //alert("removeOrder");
    console.log(order);        
    //this.orderId = order.id.replace("'", "").replace(/\"/g, "");
    let endPoint = order.id;
    this.http.delete(this.API_URL + "/orders/"+endPoint).subscribe({
      next: data => {
          this.status = 'Delete successful';
      },
      error: error => {
          this.errorMessage = error.message;
          console.error('There was an error!', error);
      }
   });
  }

  checkout(productMaps: Map, totalPrice: number): Observable<any> {
    // this.productMaps = this.cartService.computeProductMaps();
    // console.log(this.productMaps);   
    let uid: any;
    if (localStorage.getItem('My_userid')) {
      uid = localStorage.getItem('My_userid').replace("'", "").replace(/\"/g, "");
    }
    let uname: any;
    if (localStorage.getItem('My_username')) {
      uname = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }
    let order = new Order();
    order.userId = uid; 
    order.orderusername = uname;
    order.totalPrice = totalPrice;
    //let orderDetail1Ns: { productId: number, amount: number }[];
    // for (let [key, value] of productMaps) {
    //   console.log(key, value);            // productId  amount
    //   let orderDetail1N : any;
    //   // orderDetail1N.productId = key;
    //   // orderDetail1N.amount = value;
    //   orderDetail1N = {key, value};
    //   console.log("The orderDetail1N is "+orderDetail1N);
    //   orderDetail1Ns.push(orderDetail1N);
    // }
    // console.log("The orderDetail1Ns is "+orderDetail1Ns);
    //order.orderDetail1Ns = productMaps;

    console.log("order.userId = "+order.userId);
    console.log("order.orderusername = "+order.orderusername);
    console.log("order.totalPrice is "+order.totalPrice);
    console.log("order.orderDetail1Ns is "+order.orderDetail1Ns);
    console.log("The order is "+order);

    // this.saveOrder(order);
    this.saveOrder(order, productMaps)
    .subscribe((res) => {
      // console.log("res1="+res);
      // if (res.success) {
      //   alert("Saving the order succeeded!");
      // } else
      //   alert("Saving the order failed!");
    });   
    //console.log("this.createdOrderId="+this.createdOrderId);    
    // for (var i = 1; i < 1000; i++) {
    //   console.log(i * i / i / i);
    // }
    this.saveOrderDetails(productMaps);
  }

  

  saveOrder(order, productMaps: Map): Observable<any> {
      console.log("saveOrder(order): Observable<any>");
      return this.http.post(this.API_URL + "/orders", order)
      .pipe(map((res) => { 
          console.log("res2="+res);
          if (res.success) {
            console.log("The order is created!");
            console.log("res.message="+res.message);
            //localStorage.setItem('My_createdOrderId', JSON.stringify(res.message));
            //this.createdOrderId = res.message;
            //this.router.navigate(['/product']);
            this.saveOrderDetails(res.message, productMaps);
          }  
          else {
            console.log("The order is not created!");
            this.createdOrderId = "";
          }
        }));  
  }

  saveOrderDetails(createdOrderId, productMaps: Map): Observable<any> {
    // let createdOrderId: String = "";
    // if (localStorage.getItem('My_createdOrderId')) {
    //   createdOrderId = localStorage.getItem('My_createdOrderId').replace("'", "").replace(/\"/g, "");
    // }
    
    console.log("createdOrderId="+createdOrderId);
    if (createdOrderId.length > 0) {
      let product = new Product();
      for (let [key, value] of productMaps) {
            console.log(key, value);            // productId  amount
            let productSku = key;
            let amount = value;
            console.log("key, value="+key+","+value);
            // console.log("productId.length="+(productId.length));
            // console.log("amount.length="+amount.length);
            // console.log("Math.floor(amount) === amount ="+(Math.floor(amount) === amount));
            if (productSku.length > 0 && Math.floor(amount) === amount) {
              console.log("productSku, amount="+productSku+","+amount);
              let productId = 0;
              this.http.get(this.API_URL + "/productId?sku="+productSku).subscribe(data => {
                console.log(data);
                product = data;
                let productId = product.id;
                console.log("productId="+productId);
                if (Math.floor(productId) === productId && productId > 0) {
                  let orderDetailOrig = new OrderDetailOrig();
                  orderDetailOrig.orderId = createdOrderId; 
                  orderDetailOrig.productId = productId;
                  orderDetailOrig.amount = amount;
                  this.saveOrderDetail(orderDetailOrig).subscribe((res) => {
                  });
                }
              });

            }
      }           
    }
  } 

  saveOrderDetail(orderDetailOrig): Observable<any> {
    console.log(" saveOrderDetail(orderDetailOrig): Observable<any>");
    return this.http.post(this.API_URL + "/orderdetailadd", orderDetailOrig)
    .pipe(map((res) => { 
        console.log("res3="+res);
        if (res.success) {
          console.log("The orderdetail is created!");          
        }  
        else {
          console.log("The orderdetail is not created!");
          this.createdOrderId = "";
        }
      }));  
  }

}
