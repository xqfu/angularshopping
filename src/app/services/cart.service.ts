//@ts-nocheck
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CartService {
  public cartItemList: any = [];
  public productList = new BehaviorSubject<any>([]);
  public cartItemLis: any = [];
  public productLis = new BehaviorSubject<any>([]);
  public search = new BehaviorSubject<string>("");
  public productMaps = new Map();
  constructor() { }

  //  Creating All Basic Functions To Use A cross The App
  getProducts(){
    return this.productList.asObservable()
  }
  getProduct(){
    return this.productLis.asObservable()
  }

  setProduct(product: any){
    this.cartItemList.push(...product)
    this.productList.next(product)
  }
  

  addToCart(product: any){
    this.cartItemList.push(product);
    console.log(product);    
    this.productList.next(this.cartItemList);
    this.getTotalPrice();
    console.log(this.cartItemList);
  }

  getTotalPrice() : number{
    let grandTotal: any;
    grandTotal = 0.0;
    this.cartItemList.map((a:any)=>{
      grandTotal += a.unitPrice
    })
    grandTotal = grandTotal.toFixed(2);
    return grandTotal;
  }

  removeCartItem(product:any){
    this.cartItemList.map((a:any, index:any)=>{
      if(product.id === a.id){
        this.cartItemList.splice(index,1)
      }
    })
    this.productList.next(this.cartItemList)
  }

  removeAllCart(){
    this.cartItemList = [];
    this.productList.next(this.cartItemList) 
  }
  //<---------- This are functions for viewing details------------>
  setProduc(product: any){
    this.cartItemLis.push(...product)
    this.productLis.next(product)
  }

  addToCar(product: any){
    this.cartItemLis.push(product)
    this.productLis.next(this.cartItemLis)
    console.log(this.cartItemLis)
  }

  removeAllCar(){
    this.cartItemLis = [];
    this.productLis.next(this.cartItemLis) 
  }
//---------------- End of view detais function------------------>

  // public getCartAllProducts(userName: string): Observable<Product[]> {
  //   return this.http.post<Product[]>(`${this.API_URL}/cart-products`, {userName: userName}).pipe(
  //     catchError(this.errorHandler)
  //   )
  // }

  // public getCartAllProductsAmount(userName: string): Observable<ProductCart[]> {
  //   return this.http.post<ProductCart[]>(`${this.API_URL}/cart-products-length`, {userName: userName}).pipe(
  //     catchError(this.errorHandler)
  //   )
  // }

  computeProductMaps(): Map {
    this.productMaps.clear();
    this.cartItemList.map((a:any)=>{
      let productSku = a.sku;
      if (this.productMaps.has(productSku)) {
        this.productMaps.set(productSku, this.productMaps.get(productSku)+1);
      } else
        this.productMaps.set(productSku, 1);
    })
    // for (let [key, value] of this.productMaps) {
    //   console.log(key, value);   
    //   //localStorage.setItem(key, value);         
    // }
    // localStorage.setItem("My_productsmap", this.productMaps);         
    return this.productMaps;
  }
}
