//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public users: User[];
  public my_username : string = "";
  public my_userrole : string = "";

  constructor(public authService: AuthService,
    public router: Router) { }
  
  // async getUserRole() {
  //     userRole = await this.authService.getCurrentUserRole();
  // }

  ngOnInit() {
   
    let userRole: string = "";
    if (localStorage.getItem('My_userrole')) {
      userRole = localStorage.getItem('My_userrole').replace("'", "").replace(/\"/g, "");
    }
    console.log("userRole="+userRole);
    this.my_userrole = userRole;
    if (localStorage.getItem('My_username')) {
      this.my_username = localStorage.getItem('My_username').replace("'", "").replace(/\"/g, "");
    }
    //this.authService.getCurrentUserRole().subscribe(data => this.data = data);
    //userRole = await this.authService.getCurrentUserRole();
    
    // // this.authService.getCurrentUserRole().subscribe(data => {  
    // //   console.log(data);
    // //   userRole = data;
    // //   console.log("userRole="+userRole);
    // // });
    if (userRole === "ROLE_ADMIN") {
      this.authService.findAll().subscribe(data => {
        this.users = data;
      });  
    } else {
      this.authService.findCurrentUser().subscribe(data => {
        this.users = data;
        console.log("data="+data);
      });  

    }


  } 

  removeUser(user: any) {
    if (confirm('Are you sure you want to delete the user?')) {
      // Save it!
        this.authService.removeUser(user);        
        window.location.reload();
    } else {
      // Do nothing!
      //alert('The user was not deleted!');
      console.log('The user was not deleted!');
    }    
  }

  editUser(username, phone, email, address1, address2, city, state, zip) {
    //console.log("product="+user.id);  user.name, user.imageUrl, user.description, user.unitPrice
    localStorage.setItem('user_username', JSON.stringify(username));
    localStorage.setItem('user_phone', JSON.stringify(phone));
    localStorage.setItem('user_email', JSON.stringify(email));  
    localStorage.setItem('user_address1', JSON.stringify(address1));
    localStorage.setItem('user_address2', JSON.stringify(address2));
    localStorage.setItem('user_city', JSON.stringify(city));
    localStorage.setItem('user_state', JSON.stringify(state));
    localStorage.setItem('user_zip', JSON.stringify(zip));
    //this.router.nagivate(['/productdetail']);
    this.router.navigateByUrl('/userdetail')
  }
}
