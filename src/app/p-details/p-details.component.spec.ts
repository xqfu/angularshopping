import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { PDetailsComponent } from './p-details.component';
import { FormsModule } from '@angular/forms';

describe('PDetailsComponent', () => {
  let component: PDetailsComponent;
  let fixture: ComponentFixture<PDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ PDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should render the text in h2 tag', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('h2').textContent).toContain('Edit the product');
  });

	it('should render button Submit', () => { //
    expect(fixture.debugElement.nativeElement.querySelector('button').textContent).toContain('Submit');
  });

  it('should click button', async(() => {
    let buttonElement = fixture.debugElement.nativeElement.querySelector('button'); 
    buttonElement.click();
    fixture.detectChanges();  
    fixture.whenStable().then(() => {
    });
  })); 
});
