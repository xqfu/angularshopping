//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProductService } from '../services/product.service';
import {Product} from 'src/app/models/product.model';
import { AppConfig } from '../services/app.config';

@Component({
  selector: 'app-p-details',
  templateUrl: './p-details.component.html',
  styleUrls: ['./p-details.component.css']
})
export class PDetailsComponent implements OnInit {  
  private API_URL = AppConfig.API_URL;
  //public my_product: any;
  public my_sku: String ="";
  public my_name: string = "";
  public my_description: string = "";
  public my_unitPrice: number = 0.0;
  public my_imageUrl: string = "";
  // Imjecting cart service
  constructor(private http: HttpClient, public pService: ProductService, public router: Router) { }
  
// getting products and adding it in oder to view detaiks
// too lazy to get new functions lol  //product.name, product.imageUrl, product.description, product.unitPrice
    ngOnInit(): void {
      if (localStorage.getItem('product_sku')) {
        this.my_sku = localStorage.getItem('product_sku').replace("'", "").replace(/\"/g, "");
      }
      console.log("this.my_sku="+this.my_sku);

      if (localStorage.getItem('product_name')) {
        this.my_name = localStorage.getItem('product_name').replace("'", "").replace(/\"/g, "");
      }
      console.log("this.my_name="+this.my_name);

      if (localStorage.getItem('product_description')) {
        this.my_description = localStorage.getItem('product_description').replace("'", "").replace(/\"/g, "");
        console.log("this.my_description1="+this.my_description);  
      }
      console.log("this.my_description2="+this.my_description);

      if (localStorage.getItem('product_unitPrice')) {
        this.my_unitPrice = localStorage.getItem('product_unitPrice').replace("'", "").replace(/\"/g, "");
      }
      console.log("this.my_unitPrice="+this.my_unitPrice);

      if (localStorage.getItem('product_imageUrl')) {
        this.my_imageUrl = localStorage.getItem('product_imageUrl').replace("'", "").replace(/\"/g, "");
        console.log("this.my_imageUrl1="+this.my_imageUrl);
      }
      console.log("this.my_imageUrl2="+this.my_imageUrl);
      // this.pService.findAll().subscribe(data => {
      //   this.my_product = data;
      // });  
      // let pidnum = Number(pid);
      // this.product=this.pService.findProductById(pidnum);
      // console.log("pidnum="+pidnum);
      // //this.product=this.http.get<product>(this.API_URL + "/productdetails?id="+pid);         
      // console.log("this.product="+this.product);
      // console.log("this.product.unitPrice="+this.product.unitPrice);
      //this.product = new Product();
      // this.http.get(this.API_URL + "/productId?sku="+this.psku).subscribe(data => {
      //   console.log(data);
      //   this.my_product = new Product(data);  
      //   this.my_name = this.my_product.name;      
      //   this.my_description = this.my_product.description;    
      //   this.my_unitPrice = this.my_product.unitPrice;      
      //   this.my_imageUrl = this.my_product.imageUrl;   
      //});       
        // this.pService.findProductBySku(this.psku).subscribe(data => {
        //   this.my_product = data;
        // });  
        
        //console.log("productId="+this.product.id);
   

    }
//product.name, product.imageUrl, product.description, product.unitPrice
    editProduct(halfProduct) {
      console.log(halfProduct);
      let product = new Product();
      product.sku = this.my_sku;      
      product.name = halfProduct.name;
      product.imageUrl = halfProduct.imageUrl;
      product.description = halfProduct.description;
      product.unitPrice = halfProduct.unitPrice;

      console.log(product);
      this.pService.editProduct(product)
      .subscribe((res) => {
          
        // if (res.success) {
        //   alert("The product is changed!");
        //   this.router.nagivate(['/productlist']);
        // } else
        // alert("The product is not changed!");
      });
      //this.router.nagivate(['/productlist']);
    }

}
